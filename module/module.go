package module

import (
	"context"

	"github.com/go-chi/chi"
	"gitlab.com/aerilyn/service-library/errors"
)

type HTTPHandlerRegistry interface {
	RegisterRoutesTo(r chi.Router) error
}

type HTTPMiddlewareRegistry interface {
	GetMiddlewares() chi.Middlewares
}

type Runner interface {
	Run(context context.Context) error
}

type ApplicationDelegate struct {
	handlerRegistries    []HTTPHandlerRegistry
	middlewareRegistries []HTTPMiddlewareRegistry
	runners              []Runner
}

func NewApplicationDelegate() *ApplicationDelegate {
	return &ApplicationDelegate{}
}

func (a *ApplicationDelegate) AddHTTPHandlerRegistry(registry HTTPHandlerRegistry) error {
	a.handlerRegistries = append(a.handlerRegistries, registry)
	return nil
}

func (a *ApplicationDelegate) ExecuteApplyRegistry(r chi.Router) (chi.Router, error) {
	var err error
	r, err = a.applyMiddlewaresToRouter(r)
	if err != nil {
		return nil, err
	}
	r, err = a.applyRoutesToRouter(r)
	if err != nil {
		return nil, err
	}
	return r, nil
}

func (a *ApplicationDelegate) applyMiddlewaresToRouter(router chi.Router) (chi.Router, error) {
	for _, registry := range a.middlewareRegistries {
		for _, m := range registry.GetMiddlewares() {
			router.Use(m)
		}
	}
	return router, nil
}

func (a *ApplicationDelegate) applyRoutesToRouter(router chi.Router) (chi.Router, error) {
	if len(a.handlerRegistries) == 0 {
		return router, errors.NewInternalSystemError().CopyWith(errors.Message("routes registries are empty"))
	}
	for _, registry := range a.handlerRegistries {
		if err := registry.RegisterRoutesTo(router); err != nil {
			return router, err
		}
	}
	return router, nil
}

func (m *ApplicationDelegate) AddHTTPMiddlewareRegistry(registry HTTPMiddlewareRegistry) error {
	m.middlewareRegistries = append(m.middlewareRegistries, registry)
	return nil
}

func (a *ApplicationDelegate) AddRunner(runner Runner) error {
	a.runners = append(a.runners, runner)
	return nil
}

//execute message subscriber
func (a *ApplicationDelegate) RunMessagePubsub(ctx context.Context) {
	go a.runRunners(ctx)
}

func (a *ApplicationDelegate) runRunners(ctx context.Context) {
	for _, runner := range a.runners {
		go func(runner Runner) {
			if err := runner.Run(ctx); err != nil {
				panic(err)
			}
		}(runner)
	}
}

// func (a *ApplicationDelegate) runRunners(ctx context.Context, wg *sync.WaitGroup, errChan chan error) {
// 	fmt.Println("masuk sini coi")
// 	for _, runner := range a.runners {
// 		wg.Add(1)
// 		go func(runner Runner, errChan chan error) {
// 			if err := runner.Run(ctx); err != nil {
// 				wg.Done()
// 				errChan <- err
// 			}
// 			wg.Done()
// 		}(runner, errChan)
// 	}
// }
