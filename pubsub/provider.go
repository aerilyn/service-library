package pubsub

import (
	"fmt"
	"time"

	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-amqp/pkg/amqp"
	"github.com/ThreeDotsLabs/watermill/message"
	"gitlab.com/aerilyn/service-library/config"
)

const (
	CloseTimeoutInSecond int64 = 30
)

func ProvideLoggerWithStdLogger(cfg *config.Config) watermill.LoggerAdapter {
	debug := false
	trace := false
	if cfg.Get(config.AMQP_DEBUG) == "1" {
		debug = false
	}
	if cfg.Get(config.AMQP_TRACE) == "1" {
		trace = false
	}
	return watermill.NewStdLogger(debug, trace)
}

func ProvideDurableQueueConfig(cfg *config.Config) amqp.Config {
	fmt.Println("connect AMQP with connection => ", cfg.Get(config.AMQP_URI))
	return amqp.NewDurableQueueConfig(cfg.Get(config.AMQP_URI))
}

func ProvideRouter(logAdapter watermill.LoggerAdapter) (*message.Router, error) {
	router, err := message.NewRouter(message.RouterConfig{
		CloseTimeout: time.Second * time.Duration(CloseTimeoutInSecond),
	}, logAdapter)

	return router, err
}
