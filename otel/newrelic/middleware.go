package newrelic

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/aerilyn/service-library/middleware"
)

const (
	headerPlatform string = "platform"
)

var (
	allowedPlatforms map[string]bool = map[string]bool{
		"android": true,
		"ios":     true,
		"web":     true,
	}
)

type NewReliMiddlewareRegistry struct {
	Connection NewRelic
}

type nrTrx struct {
	Error error
	Code  int
}

func (r *NewReliMiddlewareRegistry) GetMiddlewares() chi.Middlewares {
	return chi.Middlewares{
		HandlerNR(r.Connection),
	}
}

func NewNewReLicMiddlewareRegistry(conNr NewRelic) *NewReliMiddlewareRegistry {
	return &NewReliMiddlewareRegistry{
		Connection: conNr,
	}
}

func HandlerNR(conNr NewRelic) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
			name := fmt.Sprintf("%s %s%s", request.Method, request.URL.Path)
			// get transaction newrelic
			txn := conNr.App.StartTransaction(name)
			defer txn.End()
			txn.SetWebRequestHTTP(request)
			writer = txn.SetWebResponse(writer)
			next.ServeHTTP(writer, request)
			platform := request.Header.Get(middleware.PlatformKey)
			name = fmt.Sprintf("%s %s%s", request.Method, platform, request.URL.Path)
			txn.SetName(name)
		})
	}
}
