package newrelic

import (
	"time"

	newrelic "github.com/newrelic/go-agent/v3/newrelic"
	"gitlab.com/aerilyn/service-library/config"
)

type NewRelic struct {
	App *newrelic.Application
}

func ProvideNewRelic(cfg *config.Config) NewRelic {
	app, err := newrelic.NewApplication(
		newrelic.ConfigAppName(cfg.Get(config.NEWRELIC_APPNAME)),
		newrelic.ConfigLicense(cfg.Get(config.NEWRELIC_KEY)),
		newrelic.ConfigAppLogForwardingEnabled(true),
	)
	if err != nil {
		panic(err)
	}

	err = app.WaitForConnection(time.Second * 5)
	if err != nil {
		panic(err)
	}

	return NewRelic{
		App: app,
	}

}
