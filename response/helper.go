package response

import (
	"github.com/go-chi/render"
	"gitlab.com/aerilyn/service-library/httphelper"
	"gitlab.com/aerilyn/service-library/log"
)

func Response(opts log.ResponseOpts) {
	if opts.Err != nil {
		httphelper.RenderCodedError(opts)
		return
	}
	log.WithCTX(opts.Ctx).PrintResponse(opts)
	render.Status(opts.Request, opts.StatusCode)
	render.JSON(opts.Writer, opts.Request, opts.Result)
	return
}
