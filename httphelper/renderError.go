package httphelper

import (
	"context"
	"net/http"

	"github.com/go-chi/render"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"
)

var presetErrorToHttpStatusMapping = map[int]int{
	errors.NewInvalidParameterError().Code():               http.StatusBadRequest,
	errors.NewInternalSystemError().Code():                 http.StatusInternalServerError,
	errors.NewEntityNotFound().Code():                      http.StatusNotFound,
	errors.NewForbiddenAccessError().Code():                http.StatusForbidden,
	errors.NewAuthorizationError().Code():                  http.StatusUnauthorized,
	errors.NewIdempotentTransactionViolationError().Code(): http.StatusConflict,
}

func RenderCodedError(opts log.ResponseOpts) {
	code := presetErrorToHttpStatusMapping[opts.Err.Code()]
	if code != 0 {
		RenderCodedErrorUseStatus(opts, code, opts.Err)
		return
	}
	switch v := opts.Err.(type) {
	case errors.CodedFieldsError:
		RenderCodedErrorUseStatus(opts, http.StatusBadRequest, v)
		return
	default:
		RenderCodedErrorUseStatus(opts, http.StatusInternalServerError, v)
		return
	}
}

func RenderCodedErrorUseStatus(responseOpts log.ResponseOpts, status int, err errors.CodedError) {
	responseOpts.StatusCode = status
	responseOpts.Err = err
	log.WithCTX(responseOpts.Ctx).PrintResponse(responseOpts)
	req := responseOpts.Request.WithContext(context.WithValue(
		responseOpts.Request.Context(), render.StatusCtxKey, status))
	render.JSON(responseOpts.Writer, req, err)
	return
}
