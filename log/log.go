package log

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httputil"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/middleware"
)

// UTCFormatter ...
type UTCFormatter struct {
	logrus.Formatter
}

type RequestData struct {
	RequestMethod string    `json:"requestMethod"`
	RequestHeader string    `json:"requestHeader"`
	Time          time.Time `json:"time"`
}

type EndpointData struct {
	RequestMethod string    `json:"requestMethod"`
	Endpoint      string    `json:"endpoint"`
	Time          time.Time `json:"time"`
}

type LogRequestData struct {
	DefaultFields
	RequestData
}

type LogEndpointData struct {
	DefaultFields
	EndpointData
}

type InfoData struct {
	Message string    `json:"message"`
	Time    time.Time `json:"time"`
}

type LogInfoData struct {
	DefaultFields
	InfoData
}

type ResponseData struct {
	StatusCode int       `json:"StatusCode"`
	Response   string    `json:"Response"`
	ExecTime   float64   `json:"ExecutionTime"`
	Time       time.Time `json:"time"`
}

type LogResponseData struct {
	DefaultFields
	ResponseData
}

type DefaultFields struct {
	RequestID string      `json:"id"`
	Filename  string      `json:"xfilename"`
	Function  string      `json:"xfunction"`
	PkgPath   string      `json:"xpackage"`
	UserID    interface{} `json:"customerID"`
	Service   string      `json:"service"`
	Platform  string      `json:"platform"`
}

type ResponseOpts struct {
	Writer     http.ResponseWriter
	Request    *http.Request
	Ctx        context.Context
	Result     interface{}
	StatusCode int
	Err        errors.CodedError
}

type Log interface {
}

type Logger struct {
	Data DefaultFields
}

func WithCTX(ctx context.Context) *Logger {
	val := getDefaultFieldValue()
	defaultFields := DefaultFields{
		Filename: val.Filename,
		Function: val.Function,
		PkgPath:  val.PkgPath,
	}
	requestID := ctx.Value(middleware.RequestIDKey)
	if requestID != nil {
		defaultFields.RequestID = requestID.(string)
	}

	serviceName := ctx.Value(middleware.ServiceNameKey)
	if serviceName != nil {
		defaultFields.Service = serviceName.(string)
	}

	platform := ctx.Value(middleware.PlatformKey)
	if platform != nil {
		defaultFields.Platform = platform.(string)
	}

	if platform != middleware.PlatformSubscriber {
		userID := ctx.Value(middleware.UserIDKey)
		if userID != nil {
			defaultFields.UserID = userID.(string)
		}
	}

	return &Logger{
		Data: defaultFields,
	}
}

func WithRequestID(uuid, platform string, cfg config.Config) *Logger {
	val := getDefaultFieldValue()
	defaultFields := DefaultFields{
		Filename: val.Filename,
		Function: val.Function,
		PkgPath:  val.PkgPath,
	}
	defaultFields.RequestID = uuid
	defaultFields.Service = cfg.Get(config.SERVICE_NAME)
	defaultFields.Platform = platform

	return &Logger{
		Data: defaultFields,
	}
}

func I() *Logger {
	val := getDefaultFieldValue()
	return &Logger{
		Data: DefaultFields{
			Filename: val.Filename,
			Function: val.Function,
			PkgPath:  val.PkgPath,
		},
	}
}

func PrintRequest(requestID, serviceName string, requestData RequestData) {
	val := getDefaultFieldValue()
	defaultFields := DefaultFields{
		RequestID: requestID,
		Filename:  val.Filename,
		Function:  val.Function,
		PkgPath:   val.PkgPath,
		Service:   serviceName,
	}
	logrus.SetFormatter(UTCFormatter{&logrus.JSONFormatter{}})
	logrus.SetOutput(os.Stdout)
	data := LogRequestData{defaultFields, requestData}
	logrus.WithField("data", data).Info("apps")
}

func PrintRouteOnly(requestID, serviceName string, endpointData EndpointData) {
	val := getDefaultFieldValue()
	defaultFields := DefaultFields{
		RequestID: requestID,
		Filename:  val.Filename,
		Function:  val.Function,
		PkgPath:   val.PkgPath,
		Service:   serviceName,
		UserID:    nil,
	}
	logrus.SetFormatter(UTCFormatter{&logrus.JSONFormatter{}})
	logrus.SetOutput(os.Stdout)
	data := LogEndpointData{defaultFields, endpointData}
	logrus.WithField("data", data).Info("apps")
}

func (d *Logger) PrintResponse(opts ResponseOpts) {
	result := opts.Result
	if opts.Err != nil {
		result = opts.Err
	}
	byteResult, err := json.Marshal(result)
	if err != nil {
		d.Error(err)
		return
	}
	responseData := ResponseData{
		StatusCode: opts.StatusCode,
		Response:   string(byteResult),
		Time:       time.Now(),
	}
	data := LogResponseData{d.Data, responseData}
	logrus.SetFormatter(UTCFormatter{&logrus.JSONFormatter{}})
	logrus.SetOutput(os.Stdout)
	logrus.WithField("data", data).Info("apps")
}

func (d *Logger) Info(msg string) {
	infoData := InfoData{
		Message: msg,
		Time:    time.Now(),
	}
	logInfoData := LogInfoData{d.Data, infoData}
	logrus.SetFormatter(UTCFormatter{&logrus.JSONFormatter{}})
	logrus.SetOutput(os.Stdout)
	logrus.WithField("data", logInfoData).Info("apps")
}

func (d *Logger) Error(err error) {
	infoData := InfoData{
		Message: err.Error(),
		Time:    time.Now(),
	}
	logInfoData := LogInfoData{d.Data, infoData}
	logrus.SetFormatter(UTCFormatter{&logrus.JSONFormatter{}})
	logrus.SetOutput(os.Stdout)
	logrus.WithField("data", logInfoData).Error("apps")
}

func getDefaultFieldValue() DefaultFields {
	pc, file, line, ok := runtime.Caller(2)
	if !ok {
		logrus.Error("Could not get context info for logger!")
		return DefaultFields{}
	}
	filename := file[strings.LastIndex(file, "/")+1:] + ":" + strconv.Itoa(line)
	callerFunc := runtime.FuncForPC(pc)
	funcForPC := callerFunc.Name()
	fn := funcForPC[strings.LastIndex(funcForPC, ".")+1:]
	pkg := funcForPC[:strings.LastIndex(funcForPC, ".")-1]
	return DefaultFields{
		Filename: filename,
		Function: fn,
		PkgPath:  pkg,
	}
}

// DumpRequest is for get all data request header
func DumpRequest(req *http.Request) string {
	header, err := httputil.DumpRequest(req, true)
	if err != nil {
		return "cannot dump request"
	}

	trim := bytes.ReplaceAll(header, []byte("\r\n"), []byte("   "))
	return string(trim)
}
