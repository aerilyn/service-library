package watermill

import (
	"context"
	"fmt"
	"time"

	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/ThreeDotsLabs/watermill/message/router/middleware"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"
	myMiddleware "gitlab.com/aerilyn/service-library/middleware"
)

type WatermillStdMiddlewareRegistry struct {
	logger watermill.LoggerAdapter
	config config.Config
}

func NewWatermillStdMiddlewareRegistry(logger watermill.LoggerAdapter) *WatermillStdMiddlewareRegistry {
	return &WatermillStdMiddlewareRegistry{
		logger: logger,
	}
}

func (r WatermillStdMiddlewareRegistry) GetHandlerMiddlewares() []message.HandlerMiddleware {
	fmt.Print("Registering WatermillStd middleware")
	return []message.HandlerMiddleware{
		middleware.CorrelationID,
		middleware.NewIgnoreErrors([]error{
			errors.NewEntityNotFound(),
			errors.NewAuthorizationError(),
			errors.NewDuplicateIDError(),
			errors.NewInvalidParameterError(),
		}).Middleware,
		middleware.Retry{
			MaxRetries:      3,
			InitialInterval: time.Second * 3,
			// Multiplier:      1,
			Logger: r.logger,
		}.Middleware,
		middleware.Recoverer,
		InitLog(r.config),
		InitCTX(r.config),
	}
}

func InitCTX(cfg config.Config) func(message.HandlerFunc) message.HandlerFunc {
	return func(h message.HandlerFunc) message.HandlerFunc {
		return func(msg *message.Message) ([]*message.Message, error) {
			ctx := context.WithValue(msg.Context(), myMiddleware.RequestIDKey, msg.UUID)
			msg.SetContext(ctx)
			ctx = context.WithValue(msg.Context(), myMiddleware.PlatformKey, myMiddleware.PlatformSubscriber)
			msg.SetContext(ctx)
			ctx = context.WithValue(msg.Context(), myMiddleware.ServiceNameKey, cfg.Get(config.SERVICE_NAME))
			msg.SetContext(ctx)
			return h(msg)
		}
	}
}

func InitLog(cfg config.Config) func(message.HandlerFunc) message.HandlerFunc {
	return func(h message.HandlerFunc) message.HandlerFunc {
		return func(msg *message.Message) ([]*message.Message, error) {
			log.WithRequestID(msg.UUID, myMiddleware.PlatformSubscriber, cfg).Info(string(msg.Payload))
			return h(msg)
		}
	}
}
