package log

import (
	"net/http"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/google/uuid"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/log"
	"gitlab.com/aerilyn/service-library/middleware"
)

// Global middleware registry for app.

type LoggingMiddlewareRegistry struct {
	config config.Config
}

func (r LoggingMiddlewareRegistry) GetMiddlewares() chi.Middlewares {
	return chi.Middlewares{
		LogRequest(r.config),
	}
}

func NewLoggingMiddlewareRegistry(cfg config.Config) *LoggingMiddlewareRegistry {
	return &LoggingMiddlewareRegistry{
		config: cfg,
	}
}

func LogRequest(cfg config.Config) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
			requestID := uuid.New().String()
			serviceName := cfg.Get(config.SERVICE_NAME)
			request = middleware.SetWithValue(request, middleware.RequestIDKey, requestID)
			request = middleware.SetWithValue(request, middleware.ServiceNameKey, serviceName)
			requestData := log.RequestData{
				RequestMethod: request.Method,
				RequestHeader: log.DumpRequest(request),
				Time:          time.Now(),
			}
			log.PrintRequest(requestID, serviceName, requestData)
			next.ServeHTTP(writer, request)
			endpoint := chi.RouteContext(request.Context()).RoutePattern()
			endpoint = strings.Replace(endpoint, "//", "/", -1)
			endpointtData := log.EndpointData{
				RequestMethod: requestData.RequestMethod,
				Endpoint:      endpoint,
				Time:          time.Now(),
			}
			log.PrintRouteOnly(requestID, serviceName, endpointtData)
		})
	}
}
