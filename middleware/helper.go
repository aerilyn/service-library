package middleware

import (
	"context"
	"net/http"
	"strings"
)

func SetWithValue(r *http.Request, key, val string) *http.Request {
	ctx := context.WithValue(r.Context(), key, val)
	return r.WithContext(ctx)
}

func IsExcludePath(path, method string, paths []string) bool {
	for _, val := range paths {
		pathWithMethod := strings.Split(val, ":")
		if pathWithMethod[0] == method && pathWithMethod[1] == path {
			return true
		}
	}
	return false
}

func IsAdminPath(path string) bool {
	if strings.Contains(path, "/admin/") {
		return true
	}
	return false
}
