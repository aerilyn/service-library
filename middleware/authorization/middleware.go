package authorization

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/httpRequest"
	"gitlab.com/aerilyn/service-library/log"
	"gitlab.com/aerilyn/service-library/middleware"
	"gitlab.com/aerilyn/service-library/response"
)

type ResponseAuthorization struct {
	IsAdmin bool `json:"isAdmin" bson:"isAdmin"`
}

type AuthorizationMiddlewareRegistryOptions struct {
	HttpRequestHelper httpRequest.HttpRequestHelper
	Config            config.Config
}

type AuthorizationMiddlewareRegistry struct {
	Opts AuthorizationMiddlewareRegistryOptions
}

func (r *AuthorizationMiddlewareRegistry) GetMiddlewares() chi.Middlewares {
	return chi.Middlewares{
		IsAdmin(r.Opts),
	}
}

func NewAuthorizationMiddlewareRegistry(opts AuthorizationMiddlewareRegistryOptions) *AuthorizationMiddlewareRegistry {
	return &AuthorizationMiddlewareRegistry{
		Opts: opts,
	}
}

func IsAdmin(opts AuthorizationMiddlewareRegistryOptions) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
			responseAuthorization := ResponseAuthorization{}
			err := errors.NewAuthorizationError()
			path := request.URL.Path
			if middleware.IsAdminPath(path) {
				userID := ""
				getUserID := request.Context().Value(middleware.UserIDKey)
				if getUserID != nil {
					userID = getUserID.(string)
				}
				if userID == "" {
					response.Response(log.ResponseOpts{writer, request, request.Context(), nil, 0, err})
					return
				}
				header := make(map[string]string)
				header["platform"] = "internal"
				header["Authorization"] = request.Header.Get(httpRequest.AuthorizationKey)
				httpValue := httpRequest.HttpRequestHelperOpts{
					Url:    opts.Config.Get(config.HTTP_AUTHORIZATION_URL),
					Method: opts.Config.Get(config.HTTP_AUTHORIZATION_METHOD),
					Header: header,
					Body:   nil,
				}
				res, err := opts.HttpRequestHelper.Request(httpValue)
				if err != nil {
					response.Response(log.ResponseOpts{writer, request, request.Context(), nil, 0, err})
					return
				}
				if res.StatusCode != http.StatusOK {
					err = errors.NewAuthorizationError()
					response.Response(log.ResponseOpts{writer, request, request.Context(), nil, 0, err})
					return
				}
				if errDecoder := json.NewDecoder(res.Body).Decode(&responseAuthorization); errDecoder != nil {
					err = errors.NewInternalSystemError().CopyWith(errors.Message(errDecoder.Error()))
					response.Response(log.ResponseOpts{writer, request, request.Context(), nil, 0, err})
					return
				}

				if !responseAuthorization.IsAdmin {
					response.Response(log.ResponseOpts{writer, request, request.Context(), nil, 0, errors.NewForbiddenAccessError()})
					return
				}
			}

			next.ServeHTTP(writer, request)
		})
	}
}
