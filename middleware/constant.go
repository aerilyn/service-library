package middleware

const (
	RequestIDKey       string = "requestID"
	ServiceNameKey     string = "serviceName"
	UserIDKey          string = "UserID"
	PlatformKey        string = "Platform"
	PlatformSubscriber string = "subscriber"
)
