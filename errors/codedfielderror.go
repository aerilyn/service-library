package errors

import (
	"encoding/json"
	"log"
)

type (
	fieldError struct {
		FTarget  string                 `json:"target"`
		FKey     string                 `json:"messageKey"`
		FMessage string                 `json:"message"`
		FParams  map[string]interface{} `json:"values,omitempty"`
	}

	codedFieldsError struct {
		CodedError
		FFieldErrors     _fieldErrors `json:"fieldErrors"`
		publicJsonSchema []byte
		fullJsonSchema   []byte
	}

	_fieldErrors []*fieldError
)

var _ CodedError = (*codedFieldsError)(nil)
var _ CodedFieldsError = (*codedFieldsError)(nil)
var _ FieldError = (*fieldError)(nil)

func NewFieldError(target string, messageKey string, message string) FieldError {
	return &fieldError{
		FKey:     messageKey,
		FMessage: message,
		FTarget:  target,
	}
}

func NewFieldErrorWithValues(target string, messageKey string, message string, params map[string]interface{}) FieldError {
	return &fieldError{
		FKey:     messageKey,
		FMessage: message,
		FTarget:  target,
		FParams:  params,
	}
}

func NewCodedFieldsError(code int, messageKey string, message string, fieldErrors []FieldError) CodedFieldsError {

	return &codedFieldsError{
		CodedError:   NewCodedError(code, messageKey, message),
		FFieldErrors: mapFieldErrorsToStrFieldErrors(fieldErrors),
	}
}

func NewCodedFieldsErrorFromJSON(errInJSON []byte) (CodedFieldsError, error) {
	err := &codedFieldsError{
		CodedError: &codedError{},
	}
	unmarshalErr := json.Unmarshal(errInJSON, &err)
	if unmarshalErr != nil {
		log.Print(unmarshalErr)
		return nil, unmarshalErr
	}
	return err, nil
}

func (f fieldError) MessageKey() string {
	return f.FKey
}

func (f fieldError) Target() string {
	return f.FTarget
}

func (f fieldError) Message() string {
	return f.FMessage
}

func (f fieldError) Values() map[string]interface{} {
	return f.FParams
}

func (c codedFieldsError) FieldErrors() []FieldError {
	return c.FFieldErrors.toFieldErrors()
}

func (fs _fieldErrors) toFieldErrors() []FieldError {
	var fieldErrors []FieldError
	for _, f := range fs {
		fieldErrors = append(fieldErrors, f)
	}
	return fieldErrors
}

func mapFieldErrorsToStrFieldErrors(fs []FieldError) _fieldErrors {
	var fieldErrors _fieldErrors
	for _, f := range fs {
		fieldErrors = append(fieldErrors, &fieldError{
			FTarget:  f.Target(),
			FKey:     f.MessageKey(),
			FMessage: f.Message(),
			FParams:  f.Values(),
		})
	}
	return fieldErrors
}

//JSON returns json representation of the error, including all nested errors and values if any.
// DO NOT USE THIS METHOD FOR Returning error to client, use `json.Unmarshal` or `JSONEncoder.Encode` instead, this is for internal use only.
func (c *codedFieldsError) JSON() []byte {
	if c.fullJsonSchema == nil {
		jsonSchema := toMap(c, false)
		jsonBytes, _ := json.Marshal(jsonSchema)
		c.fullJsonSchema = jsonBytes
	}
	return c.fullJsonSchema
}

//MarshalJSON returns json representation of the error, this method is used for marshalling error to client
//USE THIS METHOD FOR Returning error to client,this method will be called by `json.Marshal` and `JSONEncoder.Encode`
func (c *codedFieldsError) MarshalJSON() ([]byte, error) {
	if c.publicJsonSchema == nil {
		j := toMap(c, true)
		publicJsonSchema, err := json.Marshal(j)
		if err != nil {
			return nil, err
		}

		c.publicJsonSchema = publicJsonSchema
	}
	return c.publicJsonSchema, nil
}

func (c *codedFieldsError) UnmarshalJSON(b []byte) error {
	type unmarshallerSchema struct {
		FCode        int                    `json:"code"`
		FMessageKey  string                 `json:"messageKey"`
		FMessage     string                 `json:"message"`
		FOriginalErr CodedError             `json:"previousError,omitempty"`
		FParams      map[string]interface{} `json:"values,omitempty"`
		FFieldErrors _fieldErrors           `json:"fieldErrors"`
	}

	var u unmarshallerSchema

	err := json.Unmarshal(b, &u)
	if err != nil {
		return err
	}

	c.CodedError = &codedError{
		FCode:        u.FCode,
		FMessageKey:  u.FMessageKey,
		FMessage:     u.FMessage,
		FOriginalErr: u.FOriginalErr,
		FParams:      u.FParams,
	}
	c.FFieldErrors = u.FFieldErrors
	return nil
}

func (c codedFieldsError) CopyWith(setters ...OptionsSetter) CodedError {
	codedError := c.CodedError.CopyWith(setters...)
	return &codedFieldsError{
		CodedError:   codedError,
		FFieldErrors: c.FFieldErrors,
	}
}
