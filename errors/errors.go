package errors

import (
	"encoding/json"
	"fmt"
)

type (
	codedError struct {
		FCode            int                    `json:"code"`
		FMessageKey      string                 `json:"messageKey"`
		FMessage         string                 `json:"message"`
		FOriginalErr     CodedError             `json:"previousError,omitempty"`
		FParams          map[string]interface{} `json:"values,omitempty"`
		publicJsonSchema []byte
		fullJsonSchema   []byte
	}
)

//Compile time implement checking
var _ CodedError = (*codedError)(nil)

const (
	//UnknownErrCode is code that used for representing non-CodedError instances
	UnknownErrCode   int    = 0
	UnknownErrMsgKey string = "internal.err"
)

//NewCodedError returning new CodedError type
func NewCodedError(code int, messageKey string, message string) CodedError {
	return &codedError{
		FCode:        code,
		FMessageKey:  messageKey,
		FMessage:     message,
		FOriginalErr: nil,
	}
}

//WrapCodedError wrap existing coded error within a CodedError
func WrapCodedError(previous CodedError, code int, messageKey string, message string) CodedError {
	return &codedError{
		FCode:        code,
		FMessage:     message,
		FMessageKey:  messageKey,
		FOriginalErr: previous,
	}
}

//WrapError wrap any error instance within a CodedError
func WrapError(err error, code int, messageKey string, message string) CodedError {
	switch v := err.(type) {
	case CodedError:
		return WrapCodedError(v, code, messageKey, message)
	}
	return &codedError{
		FCode:        code,
		FMessage:     message,
		FMessageKey:  messageKey,
		FOriginalErr: NewCodedError(UnknownErrCode, UnknownErrMsgKey, err.Error()),
	}
}

func NewCodedErrorFromJSON(errInJSON []byte) (CodedError, error) {
	err := &codedError{}
	unmarshalErr := json.Unmarshal(errInJSON, &err)
	if unmarshalErr != nil {
		return nil, unmarshalErr
	}
	return err, nil
}

func (e codedError) CopyWith(setters ...OptionsSetter) CodedError {
	cpy := &codedError{
		FCode:        e.FCode,
		FMessageKey:  e.FMessageKey,
		FMessage:     e.FMessage,
		FOriginalErr: e.FOriginalErr,
	}
	for _, setter := range setters {
		setter(cpy)
	}
	return cpy
}

func (e codedError) Code() int {
	return e.FCode
}

func (e codedError) Message() string {
	return e.FMessage
}

func (e codedError) MessageKey() string {
	return e.FMessageKey
}

func (e codedError) Error() string {
	return fmt.Sprint(e.FCode, ":", e.FMessage)
}

func (e codedError) PreviousError() CodedError {
	return e.FOriginalErr
}

func (e *codedError) RootError() CodedError {
	if e.FOriginalErr == nil {
		return e
	}
	return e.FOriginalErr.RootError()
}

func (e codedError) Values() map[string]interface{} {
	return e.FParams
}

//JSON returns json representation of the error, including all nested errors and values if any.
// DO NOT USE THIS METHOD FOR Returning error to client, use MarshalJSON instead.
func (e *codedError) JSON() []byte {
	if e.fullJsonSchema == nil {
		j := toMap(e, false)
		fullJsonSchema, _ := json.Marshal(j)

		e.fullJsonSchema = fullJsonSchema
	}
	return e.fullJsonSchema
}

//MarshalJSON returns json representation of the error, this method is used for marshalling error to client.
//USE THIS METHOD FOR Returning error to client,this method will be called by `json.Marshal` and `JSONEncoder.Encode.
func (e *codedError) MarshalJSON() ([]byte, error) {
	if e.publicJsonSchema == nil {
		j := toMap(e, true)
		publicJsonSchema, err := json.Marshal(j)
		if err != nil {
			return nil, err
		}

		e.publicJsonSchema = publicJsonSchema
	}
	return e.publicJsonSchema, nil
}
