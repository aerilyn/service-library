package errors

const (
	ErrCodeInternalSystemError            = 1000
	ErrCodeAuthorizationError             = 1001
	ErrCodeEntityError                    = 1100
	ErrCodeEntityNotFound                 = 1101
	ErrCodeInvalidParameter               = 1102
	ErrCodeDuplicateID                    = 1102
	ErrCodeMultipleRowResult              = 1103
	ErrCodeForbiddenAccess                = 1002
	ErrCodeIdempotentTransactionViolation = 1104
	ErrCodeUnknownError                   = UnknownErrCode
)

func NewInternalSystemError() CodedError {
	return NewCodedError(ErrCodeInternalSystemError, "err.internal.system", "internal system error")
}

func NewAuthorizationError() CodedError {
	return NewCodedError(ErrCodeAuthorizationError, "err.authorization", "invalid authorization")
}

func NewEntityError() CodedError {
	return NewCodedError(ErrCodeEntityError, "err.entity", "entity not found")
}

func NewEntityNotFound() CodedError {
	return NewCodedError(ErrCodeEntityNotFound, "err.entity.notFound", "entity not found")
}

func NewInvalidParameterError() CodedError {
	return NewCodedError(ErrCodeInvalidParameter, "err.invalid.parameter", "invalid parameters")
}

func NewDuplicateIDError() CodedError {
	return NewInvalidParameterError().CopyWith(MessageKey("err.uniqueKey.duplicate"), Message("try to create entity with same ID"))
}

func NewMultipleRowResultError() CodedError {
	return NewCodedError(ErrCodeMultipleRowResult, "err.multiple.result", "multiple result returned")
}

func NewForbiddenAccessError() CodedError {
	return NewCodedError(ErrCodeForbiddenAccess, "err.access.forbidden", "forbidden access")
}

func NewIdempotentTransactionViolationError() CodedError {
	return NewCodedError(ErrCodeIdempotentTransactionViolation, "err.idempotentTransaction.violation", "violation of idempotent transaction")
}
