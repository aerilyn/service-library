package errors

func toMap(e CodedError, excludePrevErr bool) map[string]interface{} {
	jsonSchema := map[string]interface{}{
		"code":       e.Code(),
		"messageKey": e.MessageKey(),
		"message":    e.Message(),
	}

	if codedFieldErr, ok := e.(CodedFieldsError); ok {
		jsonSchema["fieldErrors"] = mapFieldErrorsToStrFieldErrors(codedFieldErr.FieldErrors())
	}

	if e.Values() != nil {
		jsonSchema["values"] = e.Values()
	}

	if e.PreviousError() != nil && !excludePrevErr {
		jsonSchema["previousError"] = toMap(e.PreviousError(), excludePrevErr)
	}
	return jsonSchema
}
