// Code generated by MockGen. DO NOT EDIT.
// Source: provide.go

// Package mock is a generated GoMock package.
package mock

import (
	http "net/http"
	reflect "reflect"
	time "time"

	gomock "github.com/golang/mock/gomock"
	errors "gitlab.com/aerilyn/service-library/errors"
	jwt "gitlab.com/aerilyn/service-library/jwt"
)

// MockJwt is a mock of Jwt interface.
type MockJwt struct {
	ctrl     *gomock.Controller
	recorder *MockJwtMockRecorder
}

// MockJwtMockRecorder is the mock recorder for MockJwt.
type MockJwtMockRecorder struct {
	mock *MockJwt
}

// NewMockJwt creates a new mock instance.
func NewMockJwt(ctrl *gomock.Controller) *MockJwt {
	mock := &MockJwt{ctrl: ctrl}
	mock.recorder = &MockJwtMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockJwt) EXPECT() *MockJwtMockRecorder {
	return m.recorder
}

// CreateToken mocks base method.
func (m *MockJwt) CreateToken(payload map[string]interface{}, duration time.Duration) (string, errors.CodedError) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateToken", payload, duration)
	ret0, _ := ret[0].(string)
	ret1, _ := ret[1].(errors.CodedError)
	return ret0, ret1
}

// CreateToken indicates an expected call of CreateToken.
func (mr *MockJwtMockRecorder) CreateToken(payload, duration interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateToken", reflect.TypeOf((*MockJwt)(nil).CreateToken), payload, duration)
}

// ExtractToken mocks base method.
func (m *MockJwt) ExtractToken(r *http.Request) *jwt.JwtToken {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ExtractToken", r)
	ret0, _ := ret[0].(*jwt.JwtToken)
	return ret0
}

// ExtractToken indicates an expected call of ExtractToken.
func (mr *MockJwtMockRecorder) ExtractToken(r interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ExtractToken", reflect.TypeOf((*MockJwt)(nil).ExtractToken), r)
}
