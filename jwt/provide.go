package jwt

//go:generate mockgen -source=provide.go -destination=mock/provide_mock.go -package=mock

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
)

type Jwt interface {
	CreateToken(payload map[string]interface{}, duration time.Duration) (string, errors.CodedError)
	ExtractToken(r *http.Request) *JwtToken
}

type JwtOpts struct {
	Config config.ConfigEnv
}

type JwtToken struct {
	Token  string
	Config config.ConfigEnv
}

func ProvideJwt(cfg config.ConfigEnv) *JwtOpts {
	return &JwtOpts{
		Config: cfg,
	}
}

func (c JwtOpts) CreateToken(payload map[string]interface{}, duration time.Duration) (string, errors.CodedError) {
	payLoadJwt := jwt.MapClaims{}
	for key, val := range payload {
		payLoadJwt[key] = val
	}
	payLoadJwt["exp"] = time.Now().Add(duration).Unix()
	sign := jwt.NewWithClaims(jwt.SigningMethodHS256, payLoadJwt)
	res, err := sign.SignedString([]byte(c.Config.Get(config.JWT_KEY)))
	if err != nil {
		return "", errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
	}
	return res, nil
}

func (c JwtOpts) ExtractToken(r *http.Request) *JwtToken {
	jwtToken := JwtToken{
		Token:  "",
		Config: c.Config,
	}
	bearToken := r.Header.Get("Authorization")
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 {
		jwtToken.Token = strArr[1]
		return &jwtToken
	}
	return &jwtToken
}

func (c *JwtToken) VerifyToken() (*jwt.Token, errors.CodedError) {
	if c.Token == "" {
		return nil, errors.NewForbiddenAccessError()
	}
	token, err := jwt.Parse(c.Token, func(token *jwt.Token) (interface{}, error) {
		//Make sure that the token method conform to "SigningMethodHMAC"
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			errString := fmt.Sprintf("unexpected signing method: %v", token.Header["alg"])
			return nil, errors.NewInternalSystemError().CopyWith(errors.Message(errString))
		}
		return []byte(c.Config.Get(config.JWT_KEY)), nil
	})
	if err != nil {
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
	}
	return token, nil
}

func (c *JwtToken) Get() string {
	return c.Token
}
