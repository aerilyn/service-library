module gitlab.com/aerilyn/service-library

go 1.18

require (
	github.com/ThreeDotsLabs/watermill v1.3.3
	github.com/ThreeDotsLabs/watermill-amqp v1.1.4
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/cors v1.2.1
	github.com/go-chi/render v1.0.3
	github.com/go-ozzo/ozzo-validation/v4 v4.3.0
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/google/uuid v1.3.1
	github.com/joho/godotenv v1.5.1
	github.com/newrelic/go-agent/v3 v3.24.1
	github.com/sirupsen/logrus v1.9.3
)

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/cenkalti/backoff/v3 v3.2.2 // indirect
	github.com/golang/mock v1.6.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/lithammer/shortuuid/v3 v3.0.7 // indirect
	github.com/oklog/ulid v1.3.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sony/gobreaker v0.5.0 // indirect
	github.com/streadway/amqp v1.0.0 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
	google.golang.org/genproto v0.0.0-20230110181048-76db0878b65f // indirect
	google.golang.org/grpc v1.54.0 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
