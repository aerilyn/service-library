package ozzomapper

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/aerilyn/service-library/common/stringutils"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"
)

func WrapValidationError(code int, messageKey string, message string, validationErrors validation.Errors) errors.CodedFieldsError {
	fieldErrors := mapValidationErrors("", validationErrors)
	return errors.NewCodedFieldsError(code, messageKey, message, fieldErrors)

}

func mapValidationErrors(parentKey string, validationErrors validation.Errors) []errors.FieldError {
	var fieldErrors []errors.FieldError
	for field, e := range validationErrors {
		target := fmt.Sprintf("%s.%s", parentKey, field)
		if stringutils.IsBlank(parentKey) {
			target = field
		}
		validationError, ok := e.(validation.Error)
		if !ok {
			childValidationErrors := e.(validation.Errors)
			childErrors := mapValidationErrors(target, childValidationErrors)
			fieldErrors = append(fieldErrors, childErrors...)
			continue
		}
		fieldErrors = append(fieldErrors,
			errors.NewFieldErrorWithValues(target,
				formatOzzoErrorCodeToFieldErrorCode(validationError.Code()),
				renderErrorMessage(validationError.Message(), validationError.Params()),
				validationError.Params(),
			),
		)
	}
	return fieldErrors
}

func formatOzzoErrorCodeToFieldErrorCode(code string) string {
	return strings.ReplaceAll(code, "_", ".")
}

func renderErrorMessage(msg string, params map[string]interface{}) string {
	errTmpl, err := template.New("err_template").Parse(msg)
	if err != nil {
		log.I().Error(err)
		return msg
	}
	var msgBuffer bytes.Buffer
	if err := errTmpl.Execute(&msgBuffer, params); err != nil {
		log.I().Error(err)
		return msg
	}
	return msgBuffer.String()
}
