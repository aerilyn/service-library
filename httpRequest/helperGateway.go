package httpRequest

import (
	"net/http"

	"gitlab.com/aerilyn/service-library/errors"
)

type HttpRequestGatewayHelper interface {
	Request(r *http.Request, host string) (*http.Response, errors.CodedError)
}
