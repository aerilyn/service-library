package usecase

//go:generate mockgen -source=helper.go -destination=mock/helper_mock.go -package=mock

import (
	"bytes"
	"encoding/json"
	"net/http"

	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/httpRequest"
)

type HttpRequestHelper struct {
}

func NewHttpRequestHelper() *HttpRequestHelper {
	return &HttpRequestHelper{}
}

func (s *HttpRequestHelper) Request(httpValue httpRequest.HttpRequestHelperOpts) (*http.Response, errors.CodedError) {
	var marshalled []byte = nil
	var err error = nil
	client := &http.Client{}
	if httpValue.Body != nil {
		marshalled, err = json.Marshal(httpValue.Body)
		if err != nil {
			return nil, errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
		}
	}

	request, err := http.NewRequest(httpValue.Method, httpValue.Url, bytes.NewReader(marshalled))
	if err != nil {
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
	}
	for key, val := range httpValue.Header {
		request.Header.Set(key, val)
	}

	response, err := client.Do(request)
	if err != nil {
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
	}
	return response, nil
}
