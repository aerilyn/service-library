package usecase

import (
	"fmt"
	"net/http"

	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/httpRequest"
	"gitlab.com/aerilyn/service-library/middleware"
)

type HttpRequestGatewayHelper struct {
}

func NewHttpRequestGatewayHelper() *HttpRequestGatewayHelper {
	return &HttpRequestGatewayHelper{}
}

func (s *HttpRequestGatewayHelper) Request(r *http.Request, host string) (*http.Response, errors.CodedError) {
	client := &http.Client{}
	urlPath := host + r.URL.Path
	rawQuery := r.URL.RawQuery
	if rawQuery != "" {
		urlPath = fmt.Sprintf("%s?%s", urlPath, rawQuery)
	}
	request, err := http.NewRequest(r.Method, urlPath, r.Body)
	if err != nil {
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
	}

	request.Header.Set(middleware.PlatformKey, r.Header.Get(middleware.PlatformKey))
	request.Header.Set(httpRequest.AuthorizationKey, r.Header.Get(httpRequest.AuthorizationKey))
	request.Header.Set(httpRequest.ContentTypeKey, r.Header.Get(httpRequest.ContentTypeKey))

	response, err := client.Do(request)
	if err != nil {
		return nil, errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
	}
	return response, nil
}
