package aes256

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"errors"

	"gitlab.com/aerilyn/service-library/config"
	libErrors "gitlab.com/aerilyn/service-library/errors"
)

//AES256 CBC need 32char key and IV 16char key

type Aes256Opts struct {
	Config config.Config
}

type Aes256 struct {
	opts Aes256Opts
}

func ProvideAes256(opts Aes256Opts) *Aes256 {
	return &Aes256{
		opts: opts,
	}
}

func (x *Aes256) Encrypt(stringToEncrypt string) (string, libErrors.CodedError) {
	key := x.opts.Config.Get(config.KEYAES256)
	iv := x.opts.Config.Get(config.KEYAES256IV)
	if key == "" || iv == "" {
		err := errors.New("KEY or IV needed")
		return "", libErrors.NewInternalSystemError().CopyWith(libErrors.Message(err.Error()))
	}
	bKey := []byte(key)
	bIV := []byte(iv)
	bPlaintext := x.pkcs5Padding([]byte(stringToEncrypt), aes.BlockSize)
	block, err := aes.NewCipher(bKey)
	if err != nil {
		return "", libErrors.NewInternalSystemError().CopyWith(libErrors.Message(err.Error()))
	}
	ciphertext := make([]byte, len(bPlaintext))
	mode := cipher.NewCBCEncrypter(block, bIV)
	mode.CryptBlocks(ciphertext, bPlaintext)
	return base64.StdEncoding.EncodeToString(ciphertext), nil
}

func (x *Aes256) pkcs5Padding(ciphertext []byte, blockSize int) []byte {
	padding := (blockSize - len(ciphertext)%blockSize)
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func (x *Aes256) Decrypt(encryptedString string) (string, libErrors.CodedError) {
	key := x.opts.Config.Get(config.KEYAES256)
	iv := x.opts.Config.Get(config.KEYAES256IV)
	if key == "" || iv == "" {
		err := errors.New("KEY or IV needed")
		return "", libErrors.NewInternalSystemError().CopyWith(libErrors.Message(err.Error()))
	}
	bKey := []byte(key)
	bIV := []byte(iv)
	// base64 decode
	cipherTextDecoded, err := base64.StdEncoding.DecodeString(encryptedString)
	if err != nil {
		return "", libErrors.NewInternalSystemError().CopyWith(libErrors.Message(err.Error()))
	}
	// cipherTextDecoded, err := hex.DecodeString(cipherText)

	block, err := aes.NewCipher(bKey)
	if err != nil {
		return "", libErrors.NewInternalSystemError().CopyWith(libErrors.Message(err.Error()))
	}

	mode := cipher.NewCBCDecrypter(block, bIV)
	mode.CryptBlocks([]byte(cipherTextDecoded), []byte(cipherTextDecoded))
	return string(cipherTextDecoded), nil
}
