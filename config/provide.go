package config

//go:generate mockgen -source=provide.go -destination=mock/provide_mock.go -package=mock

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"

	"github.com/joho/godotenv"
)

type ConfigEnv interface {
	Get(key string) string
}

type Config struct {
	PathFile string
}

func InjectConfig() Config {
	var pathFileEnv = ""
	var basePath, _ = os.Getwd()
	var env = flag.String("env", "", "type your .env")
	flag.Parse()

	if *env != "" {
		pathFileEnv = filepath.Join(basePath, "environment/", *env)
	}

	config := Config{
		PathFile: pathFileEnv,
	}
	return config
}

func ProvideConfig(config Config) *Config {
	config.init()
	return &config
}

func (s *Config) init() {
	if s.PathFile != "" {
		err := godotenv.Load(s.PathFile)
		if err != nil {
			panic("Some error occured. Err: " + err.Error())
		}
	}
	fmt.Println("success load config file =>", s.PathFile)
}

func (s *Config) Get(key string) string {
	return os.Getenv(key)
}
