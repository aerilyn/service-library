package config

const (
	AMQP_URI                  = "AMQP_URI"
	AMQP_DEBUG                = "AMQP_DEBUG"
	AMQP_TRACE                = "AMQP_TRACE"
	HTTP_AUTHORIZATION_URL    = "HTTP_AUTHORIZATION_URL"
	HTTP_AUTHORIZATION_METHOD = "HTTP_AUTHORIZATION_METHOD"
	ENVIRONMENT               = "ENVIRONMENT"
	GMAIL_HOST                = "GMAIL_HOST"
	GMAIL_PASSWORD            = "GMAIL_PASSWORD"
	GMAIL_PORT                = "GMAIL_PORT"
	GMAIL_USERNAME            = "GMAIL_USERNAME"
	GMAIL_SENDER_NAME         = "GMAIL_SENDER_NAME"
	JWT_KEY                   = "JWT_KEY"
	KEYAES256                 = "KEYAES256"
	KEYAES256IV               = "KEYAES256IV"
	MIDDLEWARE_EXCLUDE_PATH   = "MIDDLEWARE_EXCLUDE_PATH"
	MONGODB_URL               = "MONGODB_URL"
	MONGODB_DATABASE          = "MONGODB_DATABASE"
	MYSQL_DATABASE            = "MYSQL_DATABASE"
	MYSQL_HOST                = "MYSQL_HOST"
	MYSQL_URL                 = "MYSQL_URL"
	MYSQL_USERNAME            = "MYSQL_USERNAME"
	MYSQL_PASSWORD            = "MYSQL_PASSWORD"
	NEWRELIC_APPNAME          = "NEWRELIC_APPNAME"
	NEWRELIC_KEY              = "NEWRELIC_KEY"
	PLATFORM                  = "PLATFORM"
	REDIS_HOST                = "REDIS_HOST"
	REDIS_PORT                = "REDIS_PORT"
	REDIS_DB                  = "REDIS_DB"
	REDIS_PASSWORD            = "REDIS_PASSWORD"
	SERVICE_NAME              = "SERVICE_NAME"
	SERVICE_PORT              = "SERVICE_PORT"
)
